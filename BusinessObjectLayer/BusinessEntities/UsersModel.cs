﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessObjectLayer.BusinessEntities
{
    public class UsersModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? RoleId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        [Required(ErrorMessage ="Please Enter User Name")]
        public string UserName { get; set; }
        public string PWD { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [NotMapped]
        [Required]
        [Compare("NewPassword",ErrorMessage ="Please Enter Currect Password")]
        public string CompairPassword { get; set; }
    }
}
