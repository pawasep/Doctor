﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectLayer.BusinessEntities
{
    public class CasePaperModel
    {
        public int CasePaperid { get; set; }
        public Nullable<int> patientID { get; set; }
        public string Digonostic { get; set; }
        public string Gender { get; set; }
        public string Details { get; set; }
        public string Vistited { get; set; }
        public Nullable<int> DoctorID { get; set; }
        public List<CasePaperModel> cPapers { get; set; }
        public List<PatientModel> pList { get; set; }
        public int pid { get; set; }
        public bool? IsActive { get; set; }
    }
}

