﻿using System.Collections.Generic;

namespace BusinessObjectLayer.BusinessEntities
{
    public class ProcessTypeModel
    {
        public int Id { get; set; }
        public int Name { get; set; }

        public List<ProcessTypeModel> ProcessTypes { get; set; }
    }
}
