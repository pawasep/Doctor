﻿using System.Collections.Generic;

namespace BusinessObjectLayer.BusinessEntities
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<RoleModel> Roles { get; set; }
    }
}
