﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace BusinessObjectLayer.BusinessEntities
{
    public class BookAppointmentModel
    {
        public int PatinetId { get; set; }
        [Required(ErrorMessage = "Enter The Daigonestic")]
        [Display(Name = "DigoNastic")]
        public string DigonsticType { get; set; }
        public List<object> ProcessType { get; set; }
        [Required(ErrorMessage = "Please Enter The Fess")]
        public string fees { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime NextAppointment { get; set; }
    }
}
