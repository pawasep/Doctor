﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessObjectLayer.BusinessEntities
{
    public class PatientModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime? AppointMent { get; set; }
        public string Diagnosis { get; set; }
        public bool IsActive { get; set; }
        public List<PatientModel> plist { get; set; }
        public Nullable<bool> IsDoctor { get; set; }
        public Nullable<bool> IsLaboratory { get; set; }
        [Required]
        public string Passwoard { get; set; }
    }
}
