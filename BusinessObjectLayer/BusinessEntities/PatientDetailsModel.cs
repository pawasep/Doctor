﻿using System;

namespace BusinessObjectLayer.BusinessEntities
{
    public class PatientDetailsModel
    {
        public int Id { get; set; }
        public string ProcessTypeId { get; set; }
        public string DiagnosisType{ get; set; }
        public int Fees { get; set; }
        public DateTime NextAppointment { get; set; }
        public int PatientId { get; set; }
    }
}
