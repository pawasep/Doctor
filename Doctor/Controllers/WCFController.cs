﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Doctor.ServiceReference1;
namespace Doctor.Controllers
{
    public class WCFController : Controller
    {
        // GET: WCF
        CrudClient Service1Client = new CrudClient();
        public ActionResult Index()
        {
            List<ClsPatient> clsPatients = Service1Client.clsPatients().ToList();
            return View(clsPatients);
        }

        public ActionResult Details(int? id)
        {
            ClsPatient[] clsPatients = Service1Client.PatientDetails(id);
            List<ClsPatient> list = new List<ClsPatient>();
            foreach (var item in clsPatients)
            {
                list.Add(new ClsPatient()
                {
                    Name = item.Name,
                    Address = item.Address,
                    Email = item.Email,
                    AppointmentTime = item.AppointmentTime
                });
            }
            return View(list);
        }
    }
}