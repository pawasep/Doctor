﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataProvider;
using DataProvider.ModelAbstract;
using DataProvider.CustomeModel;
using DataService.ServiceAbstract;
using BusinessObjectLayer.BusinessEntities;

namespace Doctor.Controllers
{
    public class TestJoinController : Controller
    {
        // GET: TestJoin
        ITest _itest;
        public TestJoinController(ITest test) => this._itest = test;

        public ActionResult Index()
        {
            List<PatientModel> patientModel = _itest.getList();
            if (patientModel.Count == 0)
            {
                ViewBag.ErroMsg = "Somthing Went To Wrong";
                return View();
            }
            else
            {
                return View(patientModel);
            }
        }

        public ActionResult InneJoinMethod()
        {

            return View();
        }
    }
}