﻿using BusinessObjectLayer.BusinessEntities;
using DataService.ServiceMethod;
using DataService.ServiceAbstract;
using System.Web.Mvc;
using System.Web.Security;
namespace Doctor.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        User user = new User();
        IUser _iUser;
        IEmailSendnig _emailSendnig;
        public LoginController(IUser iiuser, IEmailSendnig emailSendnig)
        {
            this._iUser = iiuser;
            this._emailSendnig = emailSendnig;
        }

        public ActionResult UserLogin()
        {
            // Session.Abandon();
            //Session.Clear();
            return View(new UsersModel());
        }

        [HttpPost]
        public ActionResult UserLogin(UsersModel users)
        {
            var usersModel = user.IsExist(users);
            if (!Equals(usersModel, null))
            {
                Session["Emaill"] = usersModel.Emaill;
                Session["RoleId"] = usersModel.RoleId;
                Session["UserId"] = usersModel.Id;
                _emailSendnig.SendEmail(users.Emaill);
                return RedirectToAction("Index", "PatientInformation");
            }
            else
            {
                ViewBag.Message = "UserID/Password Invalid";
            }

            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            //  Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("UserLogin");
        }

        public ActionResult NotAllow()
        {
            return View();
        }

        public ActionResult forgoatPassword()
        {
            return View(new UsersModel());
        }

        [HttpPost]
        public ActionResult forgoatPassword(UsersModel usersModel)
        {

            ModelState.Remove(usersModel.CompairPassword);
            //   ModelState.Remove(usersModel.UserName);/

            if (!string.IsNullOrEmpty(usersModel.Name))
            {
                int result = _iUser.ChangePassword(usersModel);
                if (result > 0)
                {
                    return RedirectToAction("Index", "PatientInformation");
                }
            }
            else
            {
                return RedirectToAction("forgoatPassword");
            }
            return View(usersModel);
        }

    }
}
