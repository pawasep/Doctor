﻿using BusinessObjectLayer.BusinessEntities;
using DataService.ServiceAbstract;
using Doctor.Filters;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using DataProvider.ModelAbstract;
using DataProvider.CustomeModel;
using DataProvider;
using System.Web.Routing;
using DataService.CommanClass;
namespace Doctor.Controllers
{

    public class PatientInformationController : Controller
    {
        // GET: PatientInformation
        private readonly IPatient _patient;
        private readonly IProcessDetails _processDetails;
        private readonly ICasePaper _casePaper;
        public PatientInformationController(IPatient patient, IProcessDetails processDetails, ICasePaper casePaper)
        {
            _processDetails = processDetails;
            _patient = patient;
            _casePaper = casePaper;
        }

        //[Route("PatientInformation/plist")]
        [AllowAnonymous]
        [CustomFilter]
        public ActionResult Index()
        {
            if (Session["UserName"] != null)
            {
                PatientModel patient = new PatientModel();
                List<PatientModel> list = _patient.PatientList();
                //ViewBag.PatientList = list;
                patient.plist = list;
                return View(patient);
            }
            else
            {
                return RedirectToAction("UserLogin", "Login");
            }
        }

        [AllowAnonymous]
        [CustomFilter]
        [HttpPost]
        public ActionResult Add(PatientModel model)
        {
            bool result = _patient.AddPatient(model);
            //new DataService.CommanClass.CommanEntity<PatientModel>().Save(model);

            if (result)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View("Index");
            }
        }
        //[Route("PatientInformation/Add")]
        [AllowAnonymous]
        [CustomFilter]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<PatientModel> list = _patient.PatientList();
            ViewBag.PatientList = list;
            PatientModel patient = _patient.EditPatientByID(id);
            return View(patient);
        }

    
        [CustomFilter]
        [HttpPost]
        public ActionResult Edit(PatientModel patient)
        {
            int result = _patient.EditPatient(patient);
            if (result == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult AppointmentName(string name)
        {
            PatientModel patient = new PatientModel();
            List<PatientModel> pList = _patient.SearchByUserName(name);
            patient.plist = pList;
            return PartialView("pv_PatientList", patient);
        }


        public ActionResult AppointmentDate(DateTime date)
        {
            PatientModel patient = new PatientModel();
            List<PatientModel> pList = _patient.SearchByAppDate(date);
            patient.plist = pList;
            return PartialView("pv_PatientList", patient);
        }

        public ActionResult Appointment(int id)
        {
            List<PatientModel> plist = _patient.BookAppintment(id);
            ViewBag.PatientName = plist;
            ProcessTypeCustomeModel obj = new ProcessTypeCustomeModel()
            {
                PatientId = id
            };
            return View(obj);
        }

        [HttpPost]
        public ActionResult Appointment(ProcessTypeCustomeModel obj)
        {
            int result = _processDetails.AddProcessType(obj);
            if (result > 0)
            {
                return RedirectToAction("Index");
                ViewBag.Msg = "Record Insert";
            }
            else
            {
                return RedirectToAction("Appointment");
            }
        }

        [HttpGet]
        public ActionResult GetCasePapere()
        {
            CasePaperModel casePaper = new CasePaperModel();
            casePaper.pList = _casePaper.PatientList();
            return View(casePaper);
        }

        [HttpPost]
        public ActionResult AddCasePaper(CasePaperModel casePaperModel)
        {
            int result = _casePaper.AddCasePaper(casePaperModel);
            if (result > 0)
            {
                ViewBag.Mag = "Info Added Successfully";
            }
            else
            {
                ViewBag.Msg = "Info Faild To Add";
            }
            return View();
        }

    }
}