﻿using System.Web;
using System.Web.Mvc;
using Doctor.Filters;
using DataService.ServiceMethod;
namespace Doctor
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new CustomFilter(new User()));
        }
    }
}
