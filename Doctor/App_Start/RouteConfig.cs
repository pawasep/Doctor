﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Doctor
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "PatientInformation", action = "Index", id = UrlParameter.Optional }
            );

        ////Cutome route
        //routes.MapRoute(
        //    name: "indexf",
        //    url: "PatientInformation/plist",
        //    defaults: new { controller = "PatientInformation", action = "Index", id = UrlParameter.Optional }
        //    );

            //routes.MapRoute(
            //    name:"AddPatient",
            //    url: "PatientInformation/Add",
            //    defaults:new { controller= "PatientInformation", action="Add",id=UrlParameter.Optional}


        }
    }
}


