﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataService.ServiceAbstract;
using DataService.ServiceMethod;
using System.Web.Mvc;
using System.Web.Routing;
using NLog;
namespace Doctor.Filters
{
    public class CustomFilter : FilterAttribute, IExceptionFilter
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            logger.Error(exception);
            filterContext.Result = new PartialViewResult { ViewName = "Error" };
            if (!filterContext.ExceptionHandled && filterContext.Exception is NullReferenceException)
            {
                filterContext.Result = new RedirectResult("~/Views/Shared/ErrorView.html");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}
