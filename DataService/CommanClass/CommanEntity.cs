﻿using System.Data.Entity;
using DataProvider;
namespace DataService.CommanClass
{
    public class CommanEntity<TEntity> where TEntity : class
    {

        public virtual long Save(TEntity entity)
        {
            using (var context = new DoctorEntities1())
            {
                dynamic entityType = entity;
                bool EditedRecord =(entityType.Id > 0);

                if (!EditedRecord)
                {
                    context.Entry(entity).State = EntityState.Added;
                }
                else
                {
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
                return entityType.id;
            }
        }

        public virtual void Delete(TEntity entity)
        {
            using (var context = new DoctorEntities1())
            {
                if (entity != null)
                {
                    context.Set<TEntity>().Remove(entity);
                }
            }
        }

        public TEntity Load<Tid>(Tid tid)
        {
            using (var context = new DoctorEntities1())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var dbSet = context.Set<TEntity>().Find(tid);
                return dbSet;
            }
        }
    }
}
