﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
using DataProvider;
using DataService.ServiceAbstract;
namespace DataService.ServiceMethod
{
    public class User : IUser
    {
        public UsersModel IsExist(UsersModel users)
        {
            using (var context = new DoctorEntities1())
            {
                var result = context.Users.Where(user => user.Name == users.UserName && user.Pwd == users.PWD && user.IsActive == true)
                    .Select(x => new UsersModel
                    {
                        UserName = x.Name,
                        PWD = x.Pwd,
                        RoleId = x.RollID
                    }).FirstOrDefault();
                return result;
            }
        }

        string IUser.GetRole(int roleId)
        {
            using (var contexst = new DoctorEntities1())
            {
                return contexst.Roles.Where(d => d.RolID == roleId).Select(v => v.Name).FirstOrDefault();
            }
        }

        public int ChangePassword(UsersModel users)
        {
            using (var contex = new DoctorEntities1())
            {
               
                DataProvider.User obj = contex.Users.Where(f => f.Name == users.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(users.Name))
                {
                    obj.Pwd = users.NewPassword;
                }
                contex.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                contex.SaveChanges();
                return 1;
            }
        }


    }
}
