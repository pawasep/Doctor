﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
using DataProvider;
using DataService.ServiceAbstract;
namespace DataService.ServiceMethod
{
    public class PatientData : IPatient
    {
        DoctorEntities1 contexst = new DoctorEntities1();

        public List<ProcessType> processTypeModels
        {
            get => contexst.ProcessTypes.ToList();
            set => contexst.ProcessTypes.ToList();
        }

        public bool AddPatient(PatientModel patientModel)
        {
            using (var contexst = new DoctorEntities1())
            {
                Patient patient = new Patient();
                patient.Name = patientModel.Name;
                patient.Address = patientModel.Address;
                patient.AppointmentTime = patientModel.AppointMent;
                patient.diagnosis = patientModel.Diagnosis;
                string dd = patientModel.Passwoard;
                string pwd = GetPwdDecode(dd);
                patient.password = pwd;
                contexst.Patients.Add(patient);
                contexst.SaveChanges();
                return true;
            }
        }

        private string GetPwdDecode(string password)
        {
            try
            {
                string EncrptKey = "2013;[pnuLIT)WebCodeExpert";
                byte[] byKey = { };
                byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
                byKey = System.Text.Encoding.UTF8.GetBytes(EncrptKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(password);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        public List<PatientModel> BookAppintment(int id)
        {
            using (var context = new DoctorEntities1())
            {
                List<PatientModel> patientName = new List<PatientModel>();

                if (id != null)
                {
                    patientName = context.Patients.Where(i => i.Id == id).Select(v => new PatientModel() { Name = v.Name, Id = v.Id }).ToList();
                }
                else
                {
                    //stmt
                }
                return patientName;
            }
        }

        public int EditPatient(PatientModel patient)
        {
            using (var contexst = new DoctorEntities1())
            {
                try
                {
                    Patient obj = contexst.Patients.Where(f => f.Id == patient.Id).SingleOrDefault();
                    if (!Equals(obj.Id, null))
                    {
                        obj.Name = patient.Name;
                        obj.Address = patient.Address;
                        obj.diagnosis = patient.Diagnosis;
                        obj.AppointmentTime = patient.AppointMent;
                        obj.password = patient.Passwoard;
                        //string pwd = patient.Passwoard;
                        //obj.password = de(pwd);
                        obj.IsActive = true;
                        contexst.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        contexst.SaveChanges();
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private string DecodePassword(string pwd)
        {
            pwd = pwd.Replace(" ", "+");
            string DecryptKey = "2013;[pnuLIT)WebCodeExpert";
            byte[] byKey = { };
            byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
            byte[] inputByteArray = new byte[pwd.Length];

            byKey = System.Text.Encoding.UTF8.GetBytes(DecryptKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(pwd.Replace(" ", "+"));
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }

        public PatientModel EditPatientByID(int id)
        {
            using (var contexst = new DoctorEntities1())
            {
                var patientModel = new PatientModel();
                if (!id.Equals(0))
                {
                    Patient patient = contexst.Patients.Where(d => d.Id == id).FirstOrDefault();
                    patientModel = new PatientModel
                    {
                        Name = patient.Name,
                        Address = patient.Address,
                        Diagnosis = patient.diagnosis,
                        AppointMent = patient.AppointmentTime,
                        Passwoard=patient.password
                        
                    };
                }
                return patientModel;
            }
        }

        public List<PatientModel> SearchByAppDate(DateTime date)
        {
            using (var context = new DoctorEntities1())
            {
                List<PatientModel> patients = new List<PatientModel>();
                try
                {
                    if (date != null)
                    {
                        patients = context.Patients.Where(x => DbFunctions.TruncateTime(x.AppointmentTime) == date).
                        Select(d => new PatientModel()
                        {
                            Name = d.Name,
                            AppointMent = d.AppointmentTime,
                            Address = d.Address,
                            Diagnosis = d.diagnosis,
                            Id = d.Id
                        }).ToList();
                    }
                    else
                    {
                        using (var contexst = new DoctorEntities1())
                        {
                            patients = contexst.Patients.Select(d => new PatientModel { Name = d.Name, Address = d.Address, AppointMent = (DateTime)d.AppointmentTime, Diagnosis = d.diagnosis, Id = d.Id }).ToList();
                            return patients;
                        }
                    }
                    return patients;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<PatientModel> SearchByUserName(string name)
        {
            using (var context = new DoctorEntities1())
            {
                List<PatientModel> patients = new List<PatientModel>();
                try
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        patients = context.Patients.Where(d => d.Name.Contains(name)).
                            Select(d => new PatientModel()
                            {
                                Name = d.Name,
                                AppointMent = d.AppointmentTime,
                                Address = d.Address,
                                Diagnosis = d.diagnosis,
                                Id = d.Id
                            }).ToList();
                    }
                    else
                    {
                        using (var contexst = new DoctorEntities1())
                        {
                            patients = contexst.Patients.Select(d => new PatientModel { Name = d.Name, Address = d.Address, AppointMent = (DateTime)d.AppointmentTime, Diagnosis = d.diagnosis, Id = d.Id }).ToList();
                            return patients;
                        }
                    }
                    return patients;
                }
                catch (Exception xe)
                {
                    throw xe;
                }
            }
        }

        public List<PatientModel> PatientList()
        {
            using (var contexst = new DoctorEntities1())
            {
                List<PatientModel> list = contexst.Patients.Select(d => new PatientModel { Name = d.Name, Address = d.Address, AppointMent = (DateTime)d.AppointmentTime, Diagnosis = d.diagnosis, Id = d.Id }).ToList();
                return list;
            }
        }

        public bool Appointment(BookAppointmentModel book)
        {
            return true;


        }


    }
}
