﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
using DataService.ServiceAbstract;
using DataProvider;
namespace DataService.ServiceMethod
{
    public class CaseStudy : ICasePaper
    {
        public int AddCasePaper(CasePaperModel casePaperModel)
        {
            using (var context = new DoctorEntities1())
            {
                CasePaper caseStudy = new CasePaper()
                {
                    patientID = casePaperModel.patientID,
                    Details = casePaperModel.Details,
                    Vistited = casePaperModel.Vistited,
                    Digonostic = casePaperModel.Digonostic,
                    Gender = casePaperModel.Gender
                };
                context.CasePapers.Add(caseStudy);
                context.SaveChanges();
                return 1;
            }
        }

        public List<PatientModel> PatientList()
        {
            using (var context = new DoctorEntities1())
            {
                List<PatientModel> patientModels = context.Patients.Select(x => new PatientModel { Name = x.Name, Id = x.Id }).ToList();
                return patientModels; 
            }
        }
    }
}
