﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
using DataService.ServiceAbstract;
using DataProvider;
namespace DataService.ServiceMethod
{
    public class clsTest : ITest
    {
        public List<PatientModel> getList()
        {
            using (var context = new DoctorEntities1())
            {
                var patientDetailsByID = context.Patients.Select(v =>
                      new PatientModel
                      {
                          Name = v.Name,
                          Address = v.Address,
                          Diagnosis = v.diagnosis
                      }).ToList();
                return patientDetailsByID;
            }
        }

        public List<CasePaperModel> getListofPatient()
        {
            using (var context = new DoctorEntities1())
            {
                var list = (from p in context.Patients
                            join cm in context.CasePapers
                            on p.Id equals cm.patientID
                            where p.IsActive == true
                            select new CasePaperModel
                            {
                                Digonostic = cm.Digonostic,
                                Gender = cm.Gender,
                                Details = cm.Details
                            }).ToList();
                return list;
            }
        }

        public List<CasePaperModel> PatientAndCasePaperList()
        {
            using (var context = new DoctorEntities1())
            {
                var list = (from p in context.Patients
                            from cm in context.CasePapers
                            where p.Id == cm.patientID
                            select new CasePaperModel
                            {
                                Details = cm.Details,
                                Digonostic = cm.Digonostic,
                                Gender = cm.Gender,
                                Vistited = cm.Vistited
                            }).ToList();

                return list;
            }
        }

        public List<CasePaperModel> GetListByID(int id)
        {
            CasePaperModel casePaperModels = new CasePaperModel();
            using (var context = new DoctorEntities1())
            {
                var list = (from p in context.Patients
                            from cm in context.CasePapers
                            where (p.Id == id && p.Id == cm.patientID)
                            select new CasePaperModel()
                            {
                                Digonostic = cm.Digonostic,
                                Details = cm.Details,
                                Gender = cm.Gender,
                                IsActive = p.IsActive
                            }).ToList();

                foreach (var i in list)
                {
                    if (i.IsActive == true)
                    {
                        casePaperModels.Vistited = i.Vistited;
                        casePaperModels.Gender = i.Gender;
                        casePaperModels.Digonostic = i.Digonostic;
                    }
                    else
                    {
                        casePaperModels.cPapers = list;
                    }
                }
                return list;
            }
        }

        public int EmpbyID(int id)
        {
            using (var context = new DoctorEntities1())
            {
                var list = from p in context.Patients
                           from cp in context.CasePapers
                           where p.Id == id && p.Id == cp.patientID
                           select new CasePaperModel()
                           {
                               Digonostic = p.diagnosis,
                               Details = cp.Details,
                               Gender = cp.Gender,
                               Vistited = cp.Vistited
                           };
                return 1;
            }
        }

        public PatientModel PatientModel()
        {
            using (var context = new DoctorEntities1())
            {
                var patientModel = from patient in context.Patients
                                   where patient.IsActive == true
                                   select new PatientModel()
                                   {
                                       Diagnosis = patient.diagnosis,
                                       Name = patient.Name,
                                       Address = patient.Address,
                                       AppointMent = patient.AppointmentTime,
                                       IsDoctor = patient.IsActive
                                   };
                return patientModel as PatientModel;
            }
        }

        public PatientModel usingIntoKeywoard()
        {
            using (var context = new DoctorEntities1())
            {
                var patient = context.Patients.Join(context.CasePapers, p => p.Id, cp => cp.patientID, (p, cp) => new { p.diagnosis, p.Address, p.Name, cp.Vistited });
                return patient as PatientModel;
            }          
        }

    }
}