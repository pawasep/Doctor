﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
namespace DataService.ServiceAbstract
{
    public interface ICasePaper
    {
       int AddCasePaper(CasePaperModel casePaperModel);
        List<PatientModel> PatientList();
    }
}
