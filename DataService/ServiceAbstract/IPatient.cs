﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;

using DataProvider;
namespace DataService.ServiceAbstract
{
    public interface IPatient
    {
        List<PatientModel> PatientList();
        bool AddPatient(PatientModel patientModel);
        PatientModel EditPatientByID(int id);
        int EditPatient(PatientModel patient);
        List<PatientModel> SearchByUserName(string name);
        List<PatientModel> SearchByAppDate(DateTime date);
        List<PatientModel> BookAppintment(int id);
        bool Appointment(BookAppointmentModel bookAppointment);
        List<ProcessType> processTypeModels { get; set; }
        
    }
}
