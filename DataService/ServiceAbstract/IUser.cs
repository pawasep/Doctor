﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
using DataProvider;

namespace DataService.ServiceAbstract
{
    public interface IUser
    {
        UsersModel IsExist(UsersModel users);
        string GetRole(int roleId);
        int ChangePassword(UsersModel usersModel);
    }
}
