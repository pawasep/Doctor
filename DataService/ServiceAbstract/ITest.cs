﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectLayer.BusinessEntities;
namespace DataService.ServiceAbstract
{
    public interface ITest
    {
        List<PatientModel> getList();
        List<CasePaperModel> getListofPatient();
        List<CasePaperModel> PatientAndCasePaperList();
        int EmpbyID(int id);
    }
}
