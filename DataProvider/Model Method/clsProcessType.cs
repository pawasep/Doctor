﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataProvider.CustomeModel;
using DataProvider.ModelAbstract;
namespace DataProvider.Model_Method
{
    public class clsProcessType : IProcessDetails
    {
        public int AddProcessType(ProcessTypeCustomeModel model)
        {
            using (var context = new DoctorEntities1())
            {
                PatientDetail process = new PatientDetail()
                {
                    DiagnosisType = model.DiagnosisType,
                    Fees = model.Fees,
                    ProcessTypeId = model.ProcessTypeId,
                    NextAppointment = model.NextAppointment,
                    PatientId = model.PatientId,
                };
                context.PatientDetails.Add(process);
                context.SaveChanges();
                return 1;
            }

        }

        public List<ProcessType> list()
        {
            using (var context = new DoctorEntities1())
            {
                ProcessTypeCustomeModel plist = new ProcessTypeCustomeModel();
                plist.List = context.ProcessTypes.ToList();
                return plist.List;
            }
        }
    }
}
