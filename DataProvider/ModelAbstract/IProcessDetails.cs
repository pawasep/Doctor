﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataProvider.CustomeModel;
namespace DataProvider.ModelAbstract
{
    public interface IProcessDetails
    {
        int AddProcessType(ProcessTypeCustomeModel model);
        List<ProcessType> list();
    }
}
