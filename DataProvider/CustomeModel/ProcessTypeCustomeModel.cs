﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DataProvider.CustomeModel
{
    public class ProcessTypeCustomeModel
    {

        public int Id { get; set; }
        public Nullable<int> ProcessTypeId { get; set; }
        public string DiagnosisType { get; set; }
        [Required]
        public Nullable<int> Fees { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> NextAppointment { get; set; }
        public Nullable<int> PatientId { get; set; }
        public List<ProcessType> List { get; set; }
    }
}
