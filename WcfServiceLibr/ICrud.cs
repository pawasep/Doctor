﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceLibr.ModelClass;
namespace WcfServiceLibr
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICrud" in both code and config file together.
    [ServiceContract]
    public interface ICrud
    {
        [OperationContract]
        List<ClsPatient> clsPatients();

        [OperationContract]
        bool AddPatient(ClsPatient patient);

        [OperationContract]
        List<ClsPatient> PatientDetails(int? id);

        [OperationContract]
         bool EditPatient(ClsPatient clsPatient);

        [OperationContract]
         bool DeletePatient(int? id);
        [OperationContract]
        List<ClsPatient> SearchPatient(string name); 
    }
}
