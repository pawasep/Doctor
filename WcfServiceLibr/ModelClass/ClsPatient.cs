﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfServiceLibr.ModelClass
{
    [DataContract]
    public class ClsPatient
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public Nullable<System.DateTime> AppointmentTime { get; set; }
        [DataMember]
        public string diagnosis { get; set; }
        [DataMember]
        public Nullable<bool> IsActive { get; set; }
        [DataMember]
        public Nullable<bool> IsDoctor { get; set; }
        [DataMember]
        public Nullable<bool> IsLaboratory { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string Email { get; set; }
    }
}