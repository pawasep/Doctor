﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceLibr.ModelClass;
namespace WcfServiceLibr
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Crud" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Crud.svc or Crud.svc.cs at the Solution Explorer and start debugging.
    public class Crud : ICrud
    {
        public bool AddPatient(ClsPatient patient)
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                Patient clsPatient = new Patient()
                {
                    Name = patient.Name,
                    diagnosis = patient.diagnosis,
                    Email = patient.Email,
                    Address = patient.Address
                };
                context.Patients.Add(clsPatient);
                context.SaveChanges();
                return true;
            }
        }

        public List<ClsPatient> clsPatients()
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                List<ClsPatient> clsPatients = new List<ClsPatient>();
                var list = context.Patients;
                foreach (var item in list)
                {
                    clsPatients.Add(new ClsPatient()
                    {
                        Name = item.Name,
                        diagnosis = item.diagnosis,
                        Email = item.Email,
                        Address = item.Address,
                        AppointmentTime = item.AppointmentTime

                    });
                }
                return clsPatients;
            }
        }

        public bool DeletePatient(int? id)
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                Patient patient = context.Patients.SingleOrDefault(d => d.Id == id);
                context.Patients.Remove(patient);
                context.SaveChanges();
                return true;
            }
        }

        public bool EditPatient(ClsPatient clsPatient)
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                Patient patient = new Patient()
                {
                    Name = clsPatient.Name,
                    Address = clsPatient.Address,
                    diagnosis = clsPatient.diagnosis,
                    Email = clsPatient.Email,
                    Id=clsPatient.Id
                };

                context.Entry(patient).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                return true;
            }
        }

        public List<ClsPatient> PatientDetails(int? id)
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                List<ClsPatient> clsPatients = new List<ClsPatient>();
                var list = context.Patients.Where(d => d.Id == id);
                foreach (var item in list)
                {
                    clsPatients.Add(new ClsPatient()
                    {
                        Name = item.Name,
                        Address = item.Address,
                        diagnosis = item.diagnosis,
                    });
                }
                return clsPatients;
            }
        }

        public List<ClsPatient> SearchPatient(string name)
        {
            using (DoctorEntities context = new DoctorEntities())
            {
                List<ClsPatient> clsPatients = new List<ClsPatient>();
                var list = context.Patients;
                var SerachData = list.Where(d => d.Name.Contains(name)).ToList();
                foreach (var item in SerachData)
                {
                    clsPatients.Add(new ClsPatient()
                    {
                        Name = item.Name,
                        Address = item.Address,
                        Email = item.Email,
                        AppointmentTime = item.AppointmentTime,
                        diagnosis = item.diagnosis,
                        IsActive = item.IsDoctor
                    });
                }
                return clsPatients;
            }
        }
    }
}
